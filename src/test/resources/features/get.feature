Feature: Get brand data
  As a User, I want to get a brand data
  Scenario: Get status code 200 for brand data
    Given Brand Data is saved in Transform DB
    When Product API's GET Brand ep is called with id 'BRAND_6836150' value
    Then  Response code is 200

  Scenario: Get brand data from Transform DB
    Given Brand Data is saved in Transform DB
    When Product API's GET Brand ep is called with id 'BRAND_6836150' value
    Then Response code is 200
    And Brand has required fields

  Scenario: Get brand data by not existing ID
    Given Brand Data is not saved in Transform DB
    When Product API's GET Brand ep is called with id 'BRAND_68361501' value
    Then Response code is 404
    And Massage 'Not found'

  Scenario: Get brand data by bad request
    Given Brand Data is not saved in Transform DB
    When Product API's GET Brand ep is called with id '?/BRAND_68361501' value
    Then Response code is 400
    And Massage 'Bad request'

  Scenario: Get brand metadata from Transform DB
    Given Brand Data is saved in Transform DB
    When Product API's GET Brand ep is called with id 'BRAND_6836150' value
    Then Response code is 200
    And  Brand has metadata
      | Skoop-N-Pak  |
      | BRAND_KEY_NM |
      | IMS_BRAND_CD |
    And  Brand has metadata values
      | NAT - National |
      | SKOOP-N-PAK    |



