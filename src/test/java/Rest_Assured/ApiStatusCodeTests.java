package Rest_Assured;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.hasSize;

public class ApiStatusCodeTests {
    @Test
    public void testGetStatusCodeV1() {
        given().baseUri("https://gorest.co.in/public/v2/users")
                .when().get()
                .then().statusCode(200);

        int statusCode = given().baseUri("https://gorest.co.in/public/v2/users")
                .when().get()
                .then().extract().statusCode();

        Assert.assertEquals(200, statusCode);
        System.out.printf("Status code : %s\n", statusCode);
    }
    @Test
    public void testGetStatusCodeV2() {
        Response response = (Response) given().baseUri("https://gorest.co.in/public")
                .when().get("/v2/users");

        Assert.assertEquals(response.getStatusCode(),200);
        System.out.printf("Status code : %s\n", response.getStatusCode());
    }

    @Test
    public void testGetHeaderContentTypeV1() {
        given().baseUri("http://jsonplaceholder.typicode.com/albums")
                .when().get()
                .then().contentType("application/json; charset=utf-8");

        String contentType = given().baseUri("https://gorest.co.in/public/v2/posts")
                .when().get()
                .then().extract().contentType();

        Assert.assertEquals("application/json; charset=utf-8", contentType);
        System.out.printf("Status code : %s\n", contentType);
    }

    @Test
    public void testGetHeaderContentTypeV2() {
        given().baseUri("https://gorest.co.in/public/v2/users")
                .when().get()
                .then().assertThat().contentType("application/json; charset=utf-8");

        String contentType = given().baseUri("https://gorest.co.in/public/v2/posts")
                .when().get()
                .then().extract().contentType();

        System.out.printf("Status code : %s\n", contentType);
    }
    @Test
    public void testGetUsers() {
        given()
                .baseUri("https://gorest.co.in/public/v2/users")
                .when().get()
                .then().log().all().body("data", hasSize(10));
    }

    @Test
    public void testGetUserInfoById() {
        given()
                .baseUri("https://gorest.co.in")
                .when().get("public/v2/users/4240515")
                .then().log().body().assertThat()
                .body("id", equalTo(4240515))
                .body("name", is(notNullValue()))
                .body("gender", is(notNullValue()))
                .body("status", is(notNullValue()));
    }

    @Test
    public void testPostRequest() {
        String body =
                "{\"name\" : \"Lizaveta Novikava\"," +
                        "\"email\" : \"novikliza6911112211111111111@gmail.com\"," +
                        "\"gender\" : \"female\"," +
                        "\"status\" : \"active\"}";
        int id = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer 2f7d1af4f20696a9bebbbbb7ffc84b04bdfab9aaba94d64241a41354426f8da7")
                .body(body)
                .when().post("https://gorest.co.in/public/v2/users")
                .then().statusCode(201)
                .body(is(notNullValue()))
                .extract().response().jsonPath().getInt("id");

        System.out.printf("ID : %s\n",id);
    }

    @Test
    public void testPutRequest() {
        int id = 4260559;
        String body =
                "{\"name\" : \"Lizaveta Novikava1\"," +
                        "\"email\" : \"novikliza691111221111111111111@gmail.com\"," +
                        "\"gender\" : \"female\"," +
                        "\"status\" : \"active\"}";
        int dbId = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer 2f7d1af4f20696a9bebbbbb7ffc84b04bdfab9aaba94d64241a41354426f8da7")
                .body(body)
                .when().put("https://gorest.co.in/public/v2/users/{id}",id)
                .then()
                .statusCode(200)
                .extract().response().jsonPath().getInt("id");

        Assert.assertEquals(id, dbId);
        System.out.printf("ID : %s\n",dbId);
    }

    @Test
    public void testDeleteRequest() {
        int id =  4260931;
        given().relaxedHTTPSValidation().contentType(ContentType.JSON)
                .header("Authorization", "Bearer 2f7d1af4f20696a9bebbbbb7ffc84b04bdfab9aaba94d64241a41354426f8da7")
                .when().delete("https://gorest.co.in/public/v2/users/{id}",id)
                .then().log().all()
                .statusCode(204);
    }

}

