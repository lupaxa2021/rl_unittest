package Card_Tests;

import Card_Tests.BeforeAfterTests;
import org.example.Card;
import org.example.InvalidCardPinCodeException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PriorityTests extends BeforeAfterTests {
    Card card1= new Card("Roman",100,"CREDIT","08/23","1234","1234567891234567");

    @DataProvider(name = "withDraw")
    public static Object[][] withDraw(){
        return new Object[][]{
                {new Double(50)},
                {new Double(60)},
        };
    }
    @Test(description = "Зняти кошти",dataProvider = "withDraw",priority = 1)
    public void checkWithdrawMoney(double withDraw1) throws InvalidCardPinCodeException {
        double expectedResult = withDraw1;
        double actualResult = card1.withdrawMoney(50,"1234");
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }
    @DataProvider(name = "AddBalance")
    public static Object[][] addBalance(){
        return new Object[][]{
                {new Double(50)},
                {new Double(60)},
        };
    }
    @Test(description = "Додати грощі до балансу",priority = 0,dataProvider = "AddBalance")
    public void checkReplenishBalance(double addBalance) throws  InvalidCardPinCodeException {
        double expectedResult = card1.replenishBalance(50,"1234");
        double actualResult = card1.getInitialBalance() + addBalance;
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }
}

