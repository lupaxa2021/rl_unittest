package Card_Tests;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BeforeAfterTests {
    @BeforeTest
    public void beforeTest(){
        System.out.println("before");
    }

    @AfterTest
    public void afterTest(){
        System.out.println("after");
    }
}
