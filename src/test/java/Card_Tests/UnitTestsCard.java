package Card_Tests;

import Card_Tests.BeforeAfterTests;
import org.example.Card;
import org.example.InvalidCardInfoException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class UnitTestsCard extends BeforeAfterTests {
    Card card1= new Card("Roman",100,"CREDIT","08/23","1234","1234567891234567");


    @Test(dependsOnMethods = {"checkSetIssueDate"},alwaysRun = true)
    public void checkValidIssueDate() {
        boolean expectedResult = true;
        boolean actualResult = card1.isValidIssueDate("11/2023");
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }

    @DataProvider(name = "issueDate")
    public static Object[][] issueDate(){
        return new Object[][]{
                {new String("11/2023")},
                {new String("05/2000")},
        };
    }
    @Test(dataProvider = "issueDate" )
    public void checkSetIssueDate(String issueDate) throws InvalidCardInfoException {
        String actualResult = card1.setIssueDate("11/2023");
        Assert.assertEquals(actualResult, issueDate,"Wrong");
    }
    @Test()
    public void checkSetCardType()  {
        String expectedResult = "DEBIT";
        String actualResult = card1.setCardType("Any");
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }
    @Test()
    public void checkSetPinCode() throws  InvalidCardInfoException {
        String expectedResult = "1234";
        String actualResult = card1.setPinCode("1234");
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }

    @Test()
    public void checkSetCardNumber() throws  InvalidCardInfoException {
        String expectedResult = "1234567891234567";
        String actualResult = card1.setCardNumber("1234567891234567");
        Assert.assertEquals(actualResult,expectedResult,"Wrong");
    }
}
