//package ProductApi_Tests;
//
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
//import io.restassured.http.ContentType;
//import io.restassured.response.Response;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//import java.util.List;
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.*;
//
//public class ProductApiStepDefs {
//    private final static String URL = "https://apim-pprd.corp.ad.ctc/private/ent/gen2-product-api-stage/stage/";
//    private final static String subscriptionKey = "Ocp-Apim-Subscription-Key";
//    private final static String subscriptionKeyValue = "7d41414be23d4cc9a679e776cd4dce0e";
//    private Response response;
//
//    @Given("Brand Data is saved in Transform DB1")
//    public void brandIsStoredInTheTransformedDB() {
//    }
//    @When("Product API's GET Brand ep is called with valid ID value1")
//    public void getBrand() {
//        response =  given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/BRAND_6836150");
//    }
//    @Then("Response code is 2001")
//    public void responseCode() {
//        response.then()
//                .assertThat().log().all()
//                .statusCode(200);
//    }
//
//    @Test
//    public void testAttributeId() {
//        List<BrandData> brandList  = given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/BRAND_6836150")
//                .then()
//                .assertThat().log().all()
//                .statusCode(200)
//                .extract().body().jsonPath().getList("metadata.values", BrandData.class);
//        brandList.forEach(x->Assert.assertTrue(x.getAttributeId().contains(x.getAttributeId())));
//        Assert.assertEquals(brandList.get(2).value,"NAT - National");
//    }
//    @Test
//    public void  testGETRequest() {
//                 given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/BRAND_6836150")
//                .then()
//                .assertThat().log().all()
//                .statusCode(200)
//                .body("entityId",equalTo("BRAND_6836150"))
//                .body("type",is(notNullValue()));
//    }
//
//    @Test
//    public void  testStatusCode404() {
//        given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/BRAND_68361501")
//                .then()
//                .assertThat().log().all()
//                .statusCode(404)
//                .body("message",equalTo("Not found"));
//    }
//
//    @Test
//    public void  testStatusCode400() {
//        given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/")
//                .then()
//                .assertThat().log().all()
//                .statusCode(400)
//                .body("message",equalTo("Bad request"));
//    }
//
//    @Test
//    public void  testMetadata() {
//        given().relaxedHTTPSValidation()
//                .header(subscriptionKey,subscriptionKeyValue)
//                .auth().oauth2(token)
//                .when().contentType(ContentType.JSON)
//                .get(URL+"v1/entities/BRAND_6836150")
//                .then()
//                .assertThat().log().all()
//                .statusCode(200)
//                .body("metadata.name",equalTo("Skoop-N-Pak"))
//                .body("metadata.values[0].attributeId",equalTo("BRAND_KEY_NM"))
//                .body("metadata.values[1].attributeId",equalTo("IMS_BRAND_CD"));
//    }
//}
//
//
