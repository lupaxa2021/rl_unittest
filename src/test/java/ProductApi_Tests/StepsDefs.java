package ProductApi_Tests;

import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class StepsDefs extends BrandData {
    private final static String URL = "https://apim-pprd.corp.ad.ctc/private/ent/gen2-product-api-stage/stage/";
    private final static String subscriptionKey = "Ocp-Apim-Subscription-Key";
    private final static String subscriptionKeyValue = "7d41414be23d4cc9a679e776cd4dce0e";
    private Response response;

    protected static  String token;


    @BeforeStep()
    public void getAccessToken() {
        token = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("accessTokenAcceptedVersion", 1)
                .formParam("client_id", "e3d1bfeb-0c98-4703-ab6c-3e69010b09f1")
                .formParam("client_secret", "Qv08Q~PboEDMA.yMydv8oyiy4VOPKurD5gK94bjL")
                .formParam("scope", "api://e3d1bfeb-0c98-4703-ab6c-3e69010b09f1/.default")
                .formParam("grant_type", "client_credentials")
                .when().post("https://login.microsoftonline.com/bd6704ff-1437-477c-9ac9-c30d6f5133c5/oauth2/v2.0/token")
                .then().statusCode(200)
                .extract().jsonPath().get("access_token");
    }

    @Given("Brand Data is saved in Transform DB")
    public void brandIsStoredInTheTransformedDB() {
    }
    @Given("Brand Data is not saved in Transform DB")
    public void brandDataIsNotSavedInTransformDB() {
    }


    @When("Product API's GET Brand ep is called with id {string} value")
    public void getBrand(String id) {
        response = given().relaxedHTTPSValidation()
                .baseUri(URL)
                .basePath("v1/entities/")
                .header(subscriptionKey, subscriptionKeyValue)
                .auth().oauth2(token)
                .when().contentType(ContentType.JSON)
                .get(id);
    }

    @Then("Response code is {int}")
    public void responseCode(int statusCode) {
        response.then()
                .statusCode(statusCode);

    }

    @And("Brand has required fields")
    public void responseBody() {
        response.then()
                .body("entityId",equalTo("BRAND_6836150"))
                .body("type",is(notNullValue()));
    }
    @And("Massage {string}")
    public void massageNotFound(String message) {
        response.then()
                .body("message",equalTo(message));
    }
    @And("Brand has metadata")
    public void BrandMetadata(List<String> dataTable) {
        response.then()
                .body("metadata.name",equalTo(dataTable.get(0)))
                .body("metadata.values[0].attributeId",equalTo(dataTable.get(1)))
                .body("metadata.values[1].attributeId",equalTo(dataTable.get(2)));
    }
    @And("Brand has metadata values")
    public void BrandMetadataValues(List<String> dataTable) {
        List<BrandData> brandList = response
                .then()
                .assertThat().log().all()
                .extract().body().jsonPath().getList("metadata.values", BrandData.class);
        Assert.assertEquals(brandList.get(2).value,dataTable.get(0));
        Assert.assertEquals(brandList.get(0).value,dataTable.get(1));

    }

}


