package CosmosDbClient;

import java.io.Serializable;


public class BrandData implements Serializable {

    String attributeId;
    String value;

    public String getAttributeId() {
        return attributeId;
    }

    public String getValue() {
        return value;
    }

    public BrandData() {
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public void setValue(String value) {
        this.value = value;
    }

}