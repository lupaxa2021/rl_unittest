package CosmosDbClient;

import com.azure.cosmos.ConsistencyLevel;
import com.azure.cosmos.CosmosClientBuilder;
import com.azure.cosmos.CosmosContainer;
import com.azure.cosmos.CosmosDatabase;
import com.azure.cosmos.models.CosmosQueryRequestOptions;
import com.azure.cosmos.util.CosmosPagedIterable;
import net.minidev.json.parser.JSONParser;
import org.testng.annotations.Test;

public class CosmosClient extends BrandData {
    private com.azure.cosmos.CosmosClient client;
    JSONParser jsonParser;
    private final String host = "https://corp-stage-006-vbxz-cc-general-cosmos.documents.azure.com:443/";
    private final String key = "9LMs08gHpJOgR61zIL8ldFP3k0LISWhQGjisEOfm6aQywPEoAc9J1jqhfZF5JvfOxUp65L6U2KjLIxZgVN4jXQ==";
    @Test
    public void CosmosDbClient() {
        client = new CosmosClientBuilder()
                .endpoint(host)
                .key(key)
                .consistencyLevel(ConsistencyLevel.EVENTUAL)
                .contentResponseOnWriteEnabled(true)
                .buildClient();
        jsonParser = new JSONParser();

    }
//    public CosmosDatabase getDataBase() {
//        String id = "productdb-sql";
//        client
//                .readAllDatabases()
//                .stream()
//                .filter(cosmosDatabaseProperties -> cosmosDatabaseProperties.getId().equals(id)).findFirst().isPresent());
//            return client.getDatabase(id);
//
//    }
//    public CosmosDatabase getDataBase(String id) {
//        if (client.readAllDatabases().stream().filter(cosmosDatabaseProperties -> cosmosDatabaseProperties.getId().equals(id)).findFirst().isPresent()) {
//        return client.getDatabase(id);
//        }
//        else {
//        throw new RuntimeException("Database " + id + " wasn't found");
//        }
//    }
//    public CosmosContainer getContainer(String databaseId, String collection) {
//        return getDataBase(databaseId).getContainer(collection);
//    }
@Test
    public void getDataBase() {

    CosmosDatabase db = client.getDatabase("productdb-sql");
    CosmosContainer container = db.getContainer("Entities");
    String sql = "SELECT *  FROM  c WHERE c.key = \"BRAND_6836150\" ";
    CosmosQueryRequestOptions options = new CosmosQueryRequestOptions();
    CosmosPagedIterable<BrandData> brand = container.queryItems(sql, options, BrandData.class);
        System.out.println(brand.toString());
    }
}
