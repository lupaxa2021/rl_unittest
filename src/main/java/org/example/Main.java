package org.example;

public class Main {
    public static void main(String[] args) throws InvalidCardInfoException, InvalidCardPinCodeException {
        Card card1= new Card("Roman",100,"CREDIT","08/23","1234","1234567891234567");
        card1.setCardNumber("1234567891234567");
        card1.setCardType("CREDIT");
        card1.setIssueDate("11/2023");
        card1.setPinCode("1234");
        card1.withdrawMoney(10,"1234");
        card1.replenishBalance(100,"1234");
    }
}