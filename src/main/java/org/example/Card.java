package org.example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Card {
    private String cardHolder;
    private double initialBalance;
    private String cardType;
    public String issueDate;
    private String pinCode;
    private String cardNumber;

    public Card(String cardHolder, double initialBalance, String cardType, String issueDate, String pinCode, String cardNumber) {
        this.cardHolder = cardHolder;
        this.initialBalance = initialBalance;
        this.cardType = cardType;
        this.issueDate = issueDate;
        this.pinCode = pinCode;
        this.cardNumber = cardNumber;
    }

    public static SimpleDateFormat format = new SimpleDateFormat("MM/yy");
    public static boolean isValidIssueDate(String issueDate) {
        try {
            Date parsedDate = format.parse(issueDate);
            Date currentDate = new Date();
            return parsedDate.compareTo(currentDate) >= 0;
        } catch (ParseException e) {
            return false;
        }
    }

    public String setIssueDate(String issueDate) throws InvalidCardInfoException {
        if (isValidIssueDate(issueDate)) {
            this.issueDate = issueDate;
        } else {
            throw new InvalidCardInfoException();
        }
        return issueDate;
    }
    private void setInitialBalance(double initialBalance) throws InvalidCardInfoException {
        if (initialBalance > 0) {
            this.initialBalance = initialBalance;
            System.out.println(initialBalance);
        } else {
            throw new InvalidCardInfoException();
        }
    }

    public double withdrawMoney(double withdrawAmount, String pin) throws InvalidCardPinCodeException {
        double newBalance;
        if (Objects.equals(pinCode, pin)) {
            if (withdrawAmount > 0 && Objects.equals(cardType, "DEBIT") && initialBalance > withdrawAmount) {
                newBalance = initialBalance - withdrawAmount;
            } else if ((withdrawAmount > 0 && Objects.equals(cardType, "CREDIT"))) {
                newBalance = initialBalance - withdrawAmount;
            } else {
                newBalance = initialBalance;
            }
            System.out.println(newBalance);
        } else
            throw new InvalidCardPinCodeException();
        return newBalance;
    }

    public double replenishBalance(double replenishAmount, String pin) throws InvalidCardPinCodeException {
        double newBalance;
        if (Objects.equals(pinCode, pin)) {
            if (replenishAmount > 0) {
                newBalance = initialBalance + replenishAmount;
            } else {
                newBalance = initialBalance;
            }
            System.out.println(newBalance);
        } else
            throw new InvalidCardPinCodeException();
        return newBalance;
    }

    public String setCardType(String cardType) {
        if (cardType == "CREDIT") {
            this.cardType = cardType;
        }
        else {
            this.cardType = "DEBIT";
        }
        return  this.cardType;
    }

    public String setPinCode(String pinCode) throws InvalidCardInfoException {
            if (pinCode.length() == 4) {
                this.pinCode = pinCode;
                System.out.println(pinCode);
            } else {
                throw new InvalidCardInfoException();
            }
        return  pinCode;
    }

    public String setCardNumber(String cardNumber) throws InvalidCardInfoException {
        {
            if (cardNumber.length() == 16) {
                this.cardNumber = cardNumber;
            } else {
                throw new InvalidCardInfoException();
            }
        }
        return cardNumber;
    }


    public String getCardHolder() {
        return cardHolder;
    }

    public double getInitialBalance() {
        return initialBalance;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public String getPinCode() {
        return pinCode;
    }
}
